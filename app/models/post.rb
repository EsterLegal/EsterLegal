class Post < ApplicationRecord
  belongs_to :user, optional: true

  validates :content, presence: true, length: {in: 1..140 }
end